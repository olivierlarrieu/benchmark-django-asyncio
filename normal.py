import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.settings")

django.setup()

from product.models import Product
Product.objects.all().delete()

for i in range(10000):
    Product.objects.create(name='product_{}'.format(i))
products = Product.objects.all()
print(products.count())
