import time
from django.db import models

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=30)

    def long_processing(self):
        time.sleep(3)
        return self

    def __str__(self):
        return self.name