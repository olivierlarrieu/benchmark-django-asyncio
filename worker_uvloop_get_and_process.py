import os
import asyncio
import uvloop
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.settings")
django.setup()

from product.models import Product

def get_and_process_product(pk):
    product = Product.objects.get(pk=pk)
    result = product.long_processing()
    return result

def count_products():
    products = Product.objects.all()
    print(products.count())

async def process(pk):
    result = await loop.run_in_executor(None, get_and_process_product, pk)
    print(result)

def get_loop(uvloop_enable=False):
    if uvloop_enable:
        loop = uvloop.new_event_loop()
        asyncio.set_event_loop(loop)
    else:
        loop = asyncio.get_event_loop()
    return loop

def create_tasks():
    return [asyncio.ensure_future(process(i.get('id'))) for i in Product.objects.all()[:3000].values('id')]


loop = get_loop(uvloop_enable=True)
tasks = create_tasks()
loop.run_until_complete(asyncio.gather(*tasks))
count_products()
