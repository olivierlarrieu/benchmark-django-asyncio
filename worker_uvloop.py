import os
import asyncio
import uvloop
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.settings")
django.setup()

from product.models import Product
Product.objects.all().delete()


def create(data):
    return Product.objects.create(**data)

def count_products():
    products = Product.objects.all()
    print(products.count())

async def create_product(i):
    result = await loop.run_in_executor(None, create, {'name':'product_{}'.format(i)})
    print(result)

def get_loop(uvloop_enable=False):
    if uvloop_enable:
        loop = uvloop.new_event_loop()
        asyncio.set_event_loop(loop)
    else:
        loop = asyncio.get_event_loop()
    return loop

def create_tasks():
    return [asyncio.ensure_future(create_product(i)) for i in range(10000)]


loop = get_loop(uvloop_enable=True)
tasks = create_tasks()
loop.run_until_complete(asyncio.gather(*tasks))
count_products()
